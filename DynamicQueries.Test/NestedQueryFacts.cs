using System.Collections.Generic;
using System.Linq;
using DynamicQueries.QueryParts;
using Xunit;

namespace DynamicQueries.Test
{
    public class NestedQueryFacts
    {
        [Fact]
        public void NestedQueryShouldWork()
        {
            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Refund = 2501,
                     Owner = new Owner(){
                        Name = "Mark"
                    }
                },
                new Party(){
                    Id = 2,
                    Name ="Double boom",
                    Refund = 1000,
                     Owner = new Owner(){
                        Name = "Ron"
                    }
                },
                                new Party(){
                    Id = 3,
                    Name ="Boom boom boom boom",
                    Refund = 999,
                     Owner = new Owner(){
                        Name = "Jill"
                    }
                },
                  new Party(){
                    Id = 4,
                    Name ="New years eve",
                    Refund = 12344,
                    Owner = new Owner(){
                        Name = "Beth"
                    }
                },
                   new Party(){
                    Id = 6,
                    Name ="Double boom boom",
                    Tickets = 101,
                    Owner = new Owner(){
                        Name = "Jill"
                    }
                },
            };

            var namePart = new StringQueryPart("Owner.Name", "Jill");
            var expression = namePart.GetLambda<Party>();

            System.Console.WriteLine(expression);
            var filterd = parties.Where(expression.Compile()).ToList();
            var expected = parties.Where(x => x.Owner.Name.Contains("Jill")).ToList();

            Assert.Equal(2, filterd.Count());
            Assert.Equal(2, expected.Count());
            Assert.Equal(expected.First().Id, filterd.First().Id);
        }

        [Fact]
        public void NestedQueryShouldWorkWhenPropertyIsNull()
        {
            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Refund = 2501,
                     Owner = new Owner(){
                        Name = "Mark"
                    }
                },
                new Party(){
                    Id = 2,
                    Name ="Double boom",
                    Refund = 1000,
                     Owner = new Owner(){
                        Name = "Ron"
                    }
                },
                new Party(){
                    Id = 3,
                    Name ="Boom boom boom boom",
                    Refund = 999,
                     Owner = new Owner(){
                        Name = "Jill"
                    }
                },
                  new Party(){
                    Id = 4,
                    Name ="New years eve",
                    Refund = 12344,
                    Owner = new Owner(){
                        Name = "Beth"
                    }
                },
                   new Party(){
                    Id = 6,
                    Name ="Double boom boom",
                    Tickets = 101
                },
            };

            var namePart = new StringQueryPart("Owner.Name", "Jill");
            var expression = namePart.GetLambda<Party>();

            System.Console.WriteLine(expression);
            var filterd = parties.Where(expression.Compile()).ToList();
            Assert.Equal(1, filterd.Count());
            Assert.Contains(3, filterd.Select(x => x.Id));
        }

        [Theory]
        [InlineData("Before.PartyDate", "2020.12.01", 1)]
        [InlineData("Before.PartyDate", "<2020.12.01", 1, 2)]
        [InlineData("Before.PartyDate", "<2020.12.01", 1, 2)]
        public void NestedRangeQueryShouldWorkWhenPropertyIsNull(string property, string query, params int[] ids)
        {
            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Refund = 2501,
                    Before = new Party(){
                        Id = 11,
                        PartyDate = new System.DateTime(2020, 12, 1),
                        Owner = new Owner(){
                            Name= "Jill"
                        }
                        }
                    },
                new Party()
                {
                Id = 2,
                    Name = "Double boom",
                    Refund = 1000,
                      Before = new Party(){
                        Id = 11,
                        PartyDate = new System.DateTime(2020, 02, 1)
                        }
                },
                new Party()
                {
                    Id = 3,
                    Name = "Boom boom boom boom",
                    Refund = 999,
                     Owner = new Owner()
                     {
                         Name = "Jill"
                     }
                },
                  new Party()
                {
                    Id = 4,
                    Name = "New years eve",
                    Refund = 12344,
                    Owner = new Owner()
                    {
                        Name = "Beth"
                    }
                },
                   new Party()
                {
                    Id = 6,
                    Name = "Double boom boom",
                    Tickets = 101
                },
            };

            var namePart = new DateTimeQueryPart(property, query);
            var expression = namePart.GetLambda<Party>();

            System.Console.WriteLine(expression);
            var filterd = parties.Where(expression.Compile()).ToList();
            Assert.Equal(ids, filterd.Select(x => x.Id));
        }

        [Theory]
        [InlineData("Before.Owner.Name", "Jill", 1)]
        public void TwoLevelNestedQueryFact(string property, string query, params int[] ids)
        {
            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Refund = 2501,
                    Before = new Party(){
                        Id = 11,
                        PartyDate = new System.DateTime(2020, 12, 1),
                        Owner = new Owner(){
                            Name= "Jill"
                        }
                        }
                    },
                new Party()
                {
                Id = 2,
                    Name = "Double boom",
                    Refund = 1000,
                      Before = new Party(){
                        Id = 11,
                        PartyDate = new System.DateTime(2020, 02, 1)
                        }
                },
                new Party()
                {
                    Id = 3,
                    Name = "Boom boom boom boom",
                    Refund = 999,
                     Owner = new Owner()
                     {
                         Name = "Jill"
                     }
                },
                  new Party()
                {
                    Id = 4,
                    Name = "New years eve",
                    Refund = 12344,
                    Owner = new Owner()
                    {
                        Name = "Beth"
                    }
                },
                   new Party()
                {
                    Id = 6,
                    Name = "Double boom boom",
                    Tickets = 101
                },
            };

            var namePart = new StringQueryPart(property, query);
            var expression = namePart.GetLambda<Party>();

            System.Console.WriteLine(expression);
            var filterd = parties.Where(expression.Compile()).ToList();
            Assert.Equal(ids, filterd.Select(x => x.Id));
        }
    }
}