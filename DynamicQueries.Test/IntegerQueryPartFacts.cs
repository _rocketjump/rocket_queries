using System;
using System.Collections.Generic;
using System.Linq;
using DynamicQueries.QueryParts;
using Xunit;

namespace DynamicQueries.Test
{
    public class IntegerQueryPartFacts
    {
        [Fact]
        public void IntegerEqualsQueryPartFact()
        {
            var searchPhrase = "25";
            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Tickets = 25
                },
                new Party(){
                    Id = 2,
                    Name ="Boom boom boom boom",
                    Tickets = 2500
                },
                  new Party(){
                    Id = 3,
                    Name ="New years eve",
                    Tickets = 30
                }
            };

            var ticketPart = new IntegerQueryPart("Tickets", searchPhrase);
            var expression = ticketPart.GetLambda<Party>();

            var filterd = parties.Where(expression.Compile()).ToList();

            Assert.Equal(1, filterd.Count());
            Assert.Equal(1, filterd.First().Id);
        }

        [Fact]
        public void IntegerLesserQueryPartFact()
        {
            var searchPhrase = "<100";
            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Tickets = 100
                },
                new Party(){
                    Id = 2,
                    Name ="Double boom",
                    Tickets = 101
                },
                 new Party(){
                    Id = 3,
                    Name ="Boom boom boom boom"
                },
                  new Party(){
                    Id = 4,
                    Name ="Explicit new years eve",
                    Tickets = 10
                }
            };

            var ticketPart = new IntegerQueryPart("Tickets", searchPhrase);
            var expression = ticketPart.GetLambda<Party>();

            var filterd = parties.Where(expression.Compile()).ToList();

            Assert.Equal(3, filterd.Count());
            Assert.Contains(1, filterd.Select(x => x.Id));
            Assert.Contains(4, filterd.Select(x => x.Id));
            Assert.Contains(3, filterd.Select(x => x.Id));
        }

        [Fact]
        public void IntGreaterQueryPartFact()
        {
            var searchPhrase = ">15";
            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Tickets = 120
                },
                new Party(){
                    Id = 2,
                    Name ="Double boom",
                    Tickets = 15
                },
                                new Party(){
                    Id = 3,
                    Name ="Boom boom boom boom",
                    Tickets = 5
                },
                  new Party(){
                    Id = 4,
                    Name ="New years eve"
                }
            };

            var ticketPart = new IntegerQueryPart("Tickets", searchPhrase);
            var expression = ticketPart.GetLambda<Party>();

            var filterd = parties.Where(expression.Compile()).ToList();
            Assert.Equal(2, filterd.Count());
            Assert.Contains(1, filterd.Select(x => x.Id));
            Assert.Contains(2, filterd.Select(x => x.Id));
        }

        [Fact]
        public void IntegerGreaterLesserQueryPartFact()
        {
            var searchPhrase = ">25<300";
            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Tickets = 300
                },
                new Party(){
                    Id = 2,
                    Name ="Double boom",
                    Tickets = 25
                },
                new Party(){
                    Id = 3,
                    Name ="Boom boom boom boom",
                    Tickets = 27
                },
                  new Party(){
                    Id = 4,
                    Name ="New years eve",
                    Tickets = 301
                },
                     new Party(){
                    Id = 5,
                    Name ="New years eve",
                    Tickets = 24
                }
            };

            var ticketPart = new IntegerQueryPart("Tickets", searchPhrase);
            var expression = ticketPart.GetLambda<Party>();

            var filterd = parties.Where(expression.Compile()).ToList();

            Assert.Equal(3, filterd.Count());
            Assert.Contains(1, filterd.Select(x => x.Id));
            Assert.Contains(2, filterd.Select(x => x.Id));
            Assert.Contains(3, filterd.Select(x => x.Id));
        }


    }
}