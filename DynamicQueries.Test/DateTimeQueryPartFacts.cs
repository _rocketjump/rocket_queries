using System.Collections.Generic;
using System.Linq;
using DynamicQueries.QueryParts;
using Xunit;

namespace DynamicQueries.Test
{
    public class DateTimeQueryPartFacts
    {
        [Fact]
        public void DateTimeEqualsQueryPartFact()
        {
            var searchPhrase = "2020.02.12";
            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Cost = 2501,
                    PartyDate = new System.DateTime(2020, 2, 12)
                },
                new Party(){
                    Id = 2,
                    Name ="Boom boom boom boom",
                    Cost = 2500,
                     PartyDate = new System.DateTime(2020, 2, 3)
                },
                  new Party(){
                    Id = 3,
                    Name ="New years eve",
                    Cost = 12344,
                    PartyDate = new System.DateTime(2020, 2, 1)
                }
            };

            var ticketPart = new DateTimeQueryPart("PartyDate", searchPhrase);
            var expression = ticketPart.GetLambda<Party>();

            var filterd = parties.Where(expression.Compile()).ToList();

            Assert.Equal(1, filterd.Count());
            Assert.Equal(1, filterd.First().Id);
        }

        [Theory]
        [InlineData("<2020.02.12")]
        public void DateTimeLesserQueryPartFact(string fullString)
        {
            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Cost = 2501,
                    PartyDate = new System.DateTime(2020, 2, 12)
                },
                new Party(){
                    Id = 2,
                    Name ="Boom boom boom boom",
                    Cost = 2500,
                     PartyDate = new System.DateTime(2020, 2, 13)
                },
                  new Party(){
                    Id = 3,
                    Name ="New years eve",
                    Cost = 12344,
                    PartyDate = new System.DateTime(2020, 2, 1)
                }
            };

            var ticketPart = new DateTimeQueryPart("PartyDate", fullString);
            var expression = ticketPart.GetLambda<Party>();

            var filterd = parties.Where(expression.Compile()).Select(x => x.Id).ToList();

            Assert.Equal(2, filterd.Count());
            Assert.Contains(1, filterd);
            Assert.Contains(3, filterd);
        }

        [Theory]
        [InlineData(">2020.02.12")]
        public void DateTimeGreaterQueryPartFact(string fullString)
        {
            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Cost = 2501,
                    PartyDate = new System.DateTime(2020, 2, 12)
                },
                new Party(){
                    Id = 2,
                    Name ="Boom boom boom boom",
                    Cost = 2500,
                     PartyDate = new System.DateTime(2020, 2, 13)
                },
                  new Party(){
                    Id = 3,
                    Name ="New years eve",
                    Cost = 12344,
                    PartyDate = new System.DateTime(2020, 2, 1)
                }
            };

            var ticketPart = new DateTimeQueryPart("PartyDate", fullString);
            var expression = ticketPart.GetLambda<Party>();

            var filterd = parties.Where(expression.Compile()).Select(x => x.Id).ToList();

            Assert.Equal(2, filterd.Count());
            Assert.Contains(1, filterd);
            Assert.Contains(2, filterd);
        }

        [Theory]
        [InlineData(">2020.02.12<2020.03.01")]
        public void DateTimeRangeQueryPartFact(string fullString)
        {
            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Cost = 2501,
                    PartyDate = new System.DateTime(2020, 3, 1)
                },
                new Party(){
                    Id = 2,
                    Name ="Boom boom boom boom",
                    Cost = 2500,
                     PartyDate = new System.DateTime(2020, 2, 12)
                },
                  new Party(){
                    Id = 3,
                    Name ="New years eve",
                    Cost = 12344,
                    PartyDate = new System.DateTime(2020, 2, 1)
                },
                    new Party(){
                    Id = 4,
                    Name ="New years eve",
                    Cost = 12344,
                    PartyDate = new System.DateTime(2020, 2, 15)
                }
            };

            var ticketPart = new DateTimeQueryPart("PartyDate", fullString);
            var expression = ticketPart.GetLambda<Party>();

            var filterd = parties.Where(expression.Compile()).Select(x => x.Id).ToList();

            Assert.Equal(3, filterd.Count());
            Assert.Contains(1, filterd);
            Assert.Contains(2, filterd);
            Assert.Contains(4, filterd);
        }
    }
}