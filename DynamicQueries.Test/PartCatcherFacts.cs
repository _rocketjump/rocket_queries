using DynamicQueries.Core;
using Xunit;

namespace DynamicQueries.Test
{
    public class PartCatcherFacts
    {
        [Theory]
        [InlineData("qweasd", "<qweasd>qwer")]
        [InlineData("qweasd", ">qwer<qweasd")]
        [InlineData("qweasd", "<qweasd")]
        public void CatcherShouldFindLesserPart(string expected, string full)
        {
            var partCatcher = new PartCatcher();
            var parts = partCatcher.Catch(full);

            Assert.Equal(expected, parts.Lesser);
        }

        [Theory]
        [InlineData("qwer", "<qweasd>qwer")]
        [InlineData("qwer", ">qwer<qweasd")]
        [InlineData("qwer", ">qwer")]
        public void CatcherShouldFindGreaterPart(string expected, string full)
        {
            var partCatcher = new PartCatcher();
            var parts = partCatcher.Catch(full);

            Assert.Equal(expected, parts.Greater);
        }
        
    }
}