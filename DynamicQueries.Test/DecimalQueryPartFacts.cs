using System.Collections.Generic;
using System.Linq;
using DynamicQueries.QueryParts;
using Xunit;

namespace DynamicQueries.Test
{
    public class DecimalQueryPartFacts
    {
         [Fact]
        public void DecimalEqualsQueryPartFact()
        {
            var searchPhrase = "12344";
            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Cost = 2501
                },
                new Party(){
                    Id = 2,
                    Name ="Boom boom boom boom",
                    Cost = 2500
                },
                  new Party(){
                    Id = 3,
                    Name ="New years eve",
                    Cost = 12344
                }
            };

            var ticketPart = new DecimalQueryPart("Cost", searchPhrase);
            var expression = ticketPart.GetLambda<Party>();

            var filterd = parties.Where(expression.Compile()).ToList();

            Assert.Equal(1, filterd.Count());
            Assert.Equal(3, filterd.First().Id);
        }

        [Fact]
        public void DecimalLesserQueryPartFact()
        {
            var searchPhrase = "<1000";
            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Cost = 2501
                },
                new Party(){
                    Id = 2,
                    Name ="Double boom",
                    Cost = 1000
                },
                                new Party(){
                    Id = 3,
                    Name ="Boom boom boom boom",
                    Cost = 999
                },
                  new Party(){
                    Id = 4,
                    Name ="New years eve",
                    Cost = 12344
                },
                   new Party(){
                    Id = 6,
                    Name ="Double boom boom",
                    Tickets = 101
                },
            };

            var ticketPart = new DecimalQueryPart("Cost", searchPhrase);
            var expression = ticketPart.GetLambda<Party>();

            var filterd = parties.Where(expression.Compile()).ToList();

            Assert.Equal(3, filterd.Count());
            Assert.Contains(2, filterd.Select(x => x.Id));
            Assert.Contains(3, filterd.Select(x => x.Id));
            Assert.Contains(6, filterd.Select(x => x.Id));
        }

        [Fact]
        public void DecimalGreaterQueryPartFact()
        {
            var searchPhrase = ">1000";
            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Cost = 2501
                },
                new Party(){
                    Id = 2,
                    Name ="Double boom",
                    Cost = 1000
                },
                                new Party(){
                    Id = 3,
                    Name ="Boom boom boom boom",
                    Cost = 999
                },
                  new Party(){
                    Id = 4,
                    Name ="New years eve",
                    Cost = 12344
                }
            };

            var ticketPart = new DecimalQueryPart("Cost", searchPhrase);
            var expression = ticketPart.GetLambda<Party>();

            var filterd = parties.Where(expression.Compile()).ToList();

            Assert.Equal(3, filterd.Count());
            Assert.Contains(2, filterd.Select(x => x.Id));
            Assert.Contains(1, filterd.Select(x => x.Id));
            Assert.Contains(4, filterd.Select(x => x.Id));
        }

        [Fact]
        public void DecimalGreaterLesserQueryPartFact()
        {
            var searchPhrase = ">1000<1500";
            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Cost = 999
                },
                new Party(){
                    Id = 2,
                    Name ="Double boom",
                    Cost = 1000
                },
                new Party(){
                    Id = 3,
                    Name ="Boom boom boom boom",
                    Cost = 1500
                },
                  new Party(){
                    Id = 4,
                    Name ="New years eve",
                    Cost = 1555
                },
                new Party(){
                    Id = 5,
                    Name ="New years eve",
                    Cost = 1400
                }
            };

            var ticketPart = new DecimalQueryPart("Cost", searchPhrase);
            var expression = ticketPart.GetLambda<Party>();

            var filterd = parties.Where(expression.Compile()).ToList();

            Assert.Equal(3, filterd.Count());
            Assert.Contains(2, filterd.Select(x => x.Id));
            Assert.Contains(3, filterd.Select(x => x.Id));
            Assert.Contains(5, filterd.Select(x => x.Id));
        }
    }
}