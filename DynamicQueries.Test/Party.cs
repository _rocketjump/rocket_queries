using System;
using System.Collections.Generic;

namespace DynamicQueries.Test
{
    public class Party
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Tickets { get; set; }
        public int TicketsSold { get; set; }
        public DateTime PartyDate { get; set; }
        public decimal Cost { get; set; }
        public Nullable<decimal> Refund { get; set; }
        public PartyStyle Theme { get; set; }
        public List<Participant> Participants { get; set; }

        public Owner Owner { get; set; }

        public Party Before { get; set; }
    }

    public enum PartyStyle
    {
        Whatever = 0,
        CrazyJunkHEad = 1,
        Byobbbc = 2,
    }

    public class Participant
    {
        public string Name { get; set; }
        public bool Vip { get; set; }
    }

    public class Owner
    {
        public string Name { get; set; }
    }
}