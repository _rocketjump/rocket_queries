using System;
using System.Collections.Generic;
using System.Linq;
using DynamicQueries.QueryParts;
using Xunit;

namespace DynamicQueries.Test
{
    public class NullableQueryPartFacts
    {
        [Fact]
        public void DecimalLesserQueryPartFact()
        {
            var searchPhrase = "<1000";
            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Refund = 2501
                },
                new Party(){
                    Id = 2,
                    Name ="Double boom",
                    Refund = 1000
                },
                                new Party(){
                    Id = 3,
                    Name ="Boom boom boom boom",
                    Refund = 999
                },
                  new Party(){
                    Id = 4,
                    Name ="New years eve",
                    Refund = 12344
                },
                   new Party(){
                    Id = 6,
                    Name ="Double boom boom",
                    Tickets = 101
                },
            };

            var ticketPart = new DecimalQueryPart("Refund", searchPhrase);
            var expression = ticketPart.GetLambda<Party>();

            var filterd = parties.Where(expression.Compile()).ToList();
            Assert.Equal(2, filterd.Count());
            Assert.Contains(2, filterd.Select(x => x.Id));
            Assert.Contains(3, filterd.Select(x => x.Id));
        }
    }
}