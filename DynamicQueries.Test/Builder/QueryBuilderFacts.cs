using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DynamicQueries.Builder;
using DynamicQueries.QueryParts;
using Xunit;

namespace DynamicQueries.Test.Builder
{
    public class QueryBuilderFacts
    {

        [Fact]
        public void BuilderAndShouldWork()
        {
            var searchedDate = "2020.02.12";
            var searchedNamePart = "Will";

            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Cost = 2501,
                    PartyDate = new System.DateTime(2020, 2, 12)
                },
                new Party(){
                    Id = 2,
                    Name ="Boom boom boom boom",
                    Cost = 2500,
                     PartyDate = new System.DateTime(2020, 2, 3)
                },
                  new Party(){
                    Id = 3,
                    Name ="New years eve",
                    Cost = 12344,
                    PartyDate = new System.DateTime(2020, 2, 1)
                }
            };

            var datePart = new DateTimeQueryPart("PartyDate", searchedDate);
            var namePart = new StringQueryPart("Name", searchedNamePart);

            var lambda = QueryBuilder<Party>.StartWith(datePart)
            .And(namePart)
            .AndImDone();

            var filterd = parties.Where(lambda.Compile()).ToList();

            Assert.Equal(1, filterd.Count());
            Assert.Equal(1, filterd.First().Id);
        }

        [Fact]
        public void QueryBuilderShouldFilterWithNegation()
        {
            var searchedDate = "2020.02.12";
            var searchedNamePart = "Will";

            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Cost = 2501,
                    PartyDate = new System.DateTime(2020, 2, 12)
                },
                new Party(){
                    Id = 2,
                    Name ="Boom boom boom boom",
                    Cost = 2500,
                     PartyDate = new System.DateTime(2020, 2, 3)
                },
                  new Party(){
                    Id = 3,
                    Name ="New years eve",
                    Cost = 12344,
                    PartyDate = new System.DateTime(2020, 2, 1)
                }
            };

            var datePart = new DateTimeQueryPart("PartyDate", searchedDate);
            var namePart = new StringQueryPart("Name", searchedNamePart);

            var lambda = QueryBuilder<Party>.StartWithNot(datePart)
            .AndImDone();

            var filterd = parties.Where(lambda.Compile()).ToList();

            Assert.Equal(2, filterd.Count());
            Assert.Contains(2, filterd.Select(x => x.Id));
            Assert.Contains(3, filterd.Select(x => x.Id));
        }

        [Fact]
        public void CombinedNegationShouldWork()
        {
            var searchedDate = "2020.02.12";
            var searchedNamePart = "Will";

            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Cost = 2501,
                    PartyDate = new System.DateTime(2020, 2, 12)
                },
                new Party(){
                    Id = 2,
                    Name ="Boom boom boom boom",
                    Cost = 2500,
                     PartyDate = new System.DateTime(2020, 2, 3)
                },
                  new Party(){
                    Id = 3,
                    Name ="New years eve",
                    Cost = 12344,
                    PartyDate = new System.DateTime(2020, 2, 1)
                }
            };

            var datePart = new DateTimeQueryPart("PartyDate", searchedDate);
            var namePart = new StringQueryPart("Name", searchedNamePart);

            var lambda = QueryBuilder<Party>.StartWithNot(datePart)
            .And(namePart)
            .AndImDone();

            var filterd = parties.Where(lambda.Compile()).ToList();

            Assert.Equal(0, filterd.Count());
        }

        [Fact]
        public void CombinedOrShouldWork()
        {
            var searchedDate = "2020.02.12";
            var searchedNamePart = "New years eve";

            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Cost = 2501,
                    PartyDate = new System.DateTime(2020, 2, 12)
                },
                new Party(){
                    Id = 2,
                    Name ="Boom boom boom boom",
                    Cost = 2500,
                     PartyDate = new System.DateTime(2020, 2, 3)
                },
                  new Party(){
                    Id = 3,
                    Name ="New years eve",
                    Cost = 12344,
                    PartyDate = new System.DateTime(2020, 2, 1)
                }
            };

            var datePart = new DateTimeQueryPart("PartyDate", searchedDate);
            var namePart = new StringQueryPart("Name", searchedNamePart);

            var lambda = QueryBuilder<Party>.StartWith(datePart)
            .Or(namePart)
            .AndImDone();

            var filterd = parties.Where(lambda.Compile()).ToList();

            Assert.Equal(2, filterd.Count());
            Assert.Contains(1, filterd.Select(x => x.Id));
            Assert.Contains(3, filterd.Select(x => x.Id));
        }
    }
}