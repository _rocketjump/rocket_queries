using System.Collections.Generic;
using System.Linq;
using DynamicQueries.QueryParts;
using Xunit;

namespace DynamicQueries.Test
{
    public class EnumQueryPartFacts
    {
        [Fact]
        public void EnumQueryPartFact()
        {
            var searchPhrase = "Whatever,Byobbbc";
            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Tickets = 120,
                    Theme = PartyStyle.Byobbbc
                },
                new Party(){
                    Id = 2,
                    Name ="Double boom",
                    Tickets = 15,
                    Theme = PartyStyle.CrazyJunkHEad
                },
                                new Party(){
                    Id = 3,
                    Name ="Boom boom boom boom",
                    Tickets = 5,
                    Theme = PartyStyle.CrazyJunkHEad
                },
                  new Party(){
                    Id = 4,
                    Name ="New years eve"
                }
            };

            var queryPart = new EnumQueryPart("Theme", searchPhrase);
            var result = parties.Where(queryPart.GetLambda<Party>().Compile()).ToList();

            Assert.Equal(2, result.Count());
        }
    }
}