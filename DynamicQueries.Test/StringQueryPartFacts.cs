using System.Collections.Generic;
using System.Linq;
using DynamicQueries.QueryParts;
using Xunit;

namespace DynamicQueries.Test
{
    public class StringQueryPartFacts
    {
        [Fact]
        public void StringQueryPartFact()
        {
            var searchPhrase = "boom";

            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday"
                },
                new Party(){
                    Id = 2,
                    Name ="Boom boom boom boom"
                }
            };

            var namePart = new StringQueryPart("Name", searchPhrase);
            var expression = namePart.GetLambda<Party>();

            System.Console.WriteLine(expression);
            var filterd = parties.Where(expression.Compile()).ToList();
            var expected = parties.Where(x => x.Name.Contains(searchPhrase)).ToList();

            Assert.Equal(1, filterd.Count());
            Assert.Equal(1, expected.Count());
            Assert.Equal(expected.First().Id, filterd.First().Id);
        }
    }
}