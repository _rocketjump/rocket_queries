using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DynamicQueries.Builder;
using DynamicQueries.QueryParts;
using Xunit;

namespace DynamicQueries.Test
{
    public class CombinedQueriesFacts
    {
        [Fact]
        public void CombinedQueryPartdShouldWork()
        {
            var searchedDate = "2020.02.12";
            var searchedNamePart = "Will";

            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Cost = 2501,
                    PartyDate = new System.DateTime(2020, 2, 12)
                },
                new Party(){
                    Id = 2,
                    Name ="Boom boom boom boom",
                    Cost = 2500,
                     PartyDate = new System.DateTime(2020, 2, 3)
                },
                  new Party(){
                    Id = 3,
                    Name ="New years eve",
                    Cost = 12344,
                    PartyDate = new System.DateTime(2020, 2, 1)
                }
            };

            var datePart = new DateTimeQueryPart("PartyDate", searchedDate);
            var namePart = new StringQueryPart("Name", searchedNamePart);

            var parameter = Expression.Parameter(typeof(Party));
            var nameMemberExpression = Expression.PropertyOrField(parameter, "Name");
            var dateMemberExpression = Expression.PropertyOrField(parameter, "PartyDate");

            var nameExpression = namePart.GetPredicate<Party>(nameMemberExpression);
            var dateExpression = datePart.GetPredicate<Party>(dateMemberExpression);

            var lambda = Expression.Lambda<Func<Party, bool>>(Expression.AndAlso(nameExpression, dateExpression), parameter);

            var filterd = parties.Where(lambda.Compile()).ToList();

            Assert.Equal(1, filterd.Count());
            Assert.Equal(1, filterd.First().Id);
        }
    }
}