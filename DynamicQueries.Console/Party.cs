using System;

namespace DynamicQueries.Console
{
    public class Party
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Tickets { get; set; }
        public int TicketsSold { get; set; }
        public DateTime PartyDate { get; set; }
        public PartyStyle Theme { get; set; }
        public decimal Refund { get; set; }
        public Owner Owner { get; set; }
    }

    public enum PartyStyle
    {
        Whatever = 0,
        CrazyJunkHEad = 1,
        Byobbbc = 2,
    }

    public class Owner
    {
        public string Name { get; set; }
    }
}