using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DynamicQueries.QueryParts;
using Xunit;

namespace DynamicQueries.Mvc.Test
{
    public class QuerySourceFacts
    {
        private readonly DynamicQueries.Factory _factory = new DynamicQueries.Factory();
        private Dictionary<string, Func<string, string, IQueryPart>> Definitions()
        {
            var definitions = new Dictionary<string, Func<string, string, IQueryPart>>();
            definitions.Add("cost", _factory.Get(typeof(decimal)));
            definitions.Add("name", _factory.Get(typeof(string)));
            definitions.Add("owner_Name", _factory.Get(typeof(string)));
            return definitions;
        }

        [Theory]
        [InlineData("!cost=999", 4, 1, 2, 4, 6)]
        [InlineData("cost=999", 1, 3)]
        [InlineData("!cost=999&name=Will", 1, 1)]
        [InlineData("name=Will!cost=999", 1, 1)]
        [InlineData("!cost=999!name=Will", 3, 2, 4, 6)]
        [InlineData("!name=Will!cost=999", 3, 2, 4, 6)]
        [InlineData("name=Will|cost=999", 2, 1, 3)]
        [InlineData("owner_Name=John|cost=999", 2, 4, 3)]
        [InlineData("owner_Name=John&cost=123|cost=999", 1, 3)]
        [InlineData("", 5, 1, 2, 3, 4,6)]
        public void QuerySourceShouldWork(string queryString, int results, params int[] filteredIds)
        {
            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Cost = 2501
                },
                new Party(){
                    Id = 2,
                    Name ="Double boom",
                    Cost = 1000
                },
                    new Party(){
                    Id = 3,
                    Name ="Boom boom boom boom",
                    Cost = 999
                },
                  new Party(){
                    Id = 4,
                    Name ="New years eve",
                    Cost = 12344,
                    Owner = new Person(){
                        Name = "John"
                    }
                },
                   new Party(){
                    Id = 6,
                    Name ="Double boom boom",
                    Tickets = 101,
                    Owner = new Person(){
                        Name = "Jill"
                    }
                },
            };

            var source = new DefaultQuerySource();
            Expression<Func<Party, bool>> query = null;

            source.TryGet(queryString, Definitions(), out query);

            var result = parties.Where(query.Compile()).ToList();
            Assert.Equal(results, result.Count());

            var ids = result.Select(_ => _.Id);
            foreach (var p in filteredIds)
            {
                Assert.Contains(p, ids);
            }
        }
    }


    public class Party
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Tickets { get; set; }
        public int TicketsSold { get; set; }
        public DateTime PartyDate { get; set; }
        public decimal Cost { get; set; }
        public Nullable<decimal> Refund { get; set; }
        public PartyStyle Theme { get; set; }

        public Person Owner { get; set; }
    }

    public enum PartyStyle
    {
        Whatever = 0,
        CrazyJunkHEad = 1,
        Byobbbc = 2,
    }

    public class Person
    {
        public string Name { get; set; }
        public DateTime? BirthDate { get; set; }
    }
}