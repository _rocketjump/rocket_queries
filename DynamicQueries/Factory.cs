using System;
using System.Collections.Generic;
using DynamicQueries.QueryParts;

namespace DynamicQueries
{
    public interface IFactory{
        Func<string, string, IQueryPart> Get(Type t);
    }
    public class Factory : IFactory
    {
        private readonly Dictionary<Type, Func<string, string, IQueryPart>> Mappings = new Dictionary<Type, Func<string, string, IQueryPart>>();

        ///TODo use regular types for this, not query types
        public Factory()
        {
            Mappings.Add(typeof(decimal), (property, query) =>
            {
                return new DecimalQueryPart(property, query);
            });

            Mappings.Add(typeof(decimal?), (property, query) =>
            {
                return new DecimalQueryPart(property, query);
            });

            Mappings.Add(typeof(DateTime), (property, query) =>
            {
                return new DateTimeQueryPart(property, query);
            });

            Mappings.Add(typeof(DateTime?), (property, query) =>
            {
                return new DateTimeQueryPart(property, query);
            });

            Mappings.Add(typeof(int), (property, query) =>
            {
                return new IntegerQueryPart(property, query);
            });

            Mappings.Add(typeof(int?), (property, query) =>
            {
                return new IntegerQueryPart(property, query);
            });

            Mappings.Add(typeof(string), (property, query) =>
            {
                return new StringQueryPart(property, query);
            });

            Mappings.Add(typeof(EnumQueryPart), (property, query) =>
            {
                return new EnumQueryPart(property, query);
            });
        }

        public Func<string, string, IQueryPart> Get(Type t)
        {
            Func<string, string, IQueryPart> factoryFunction = null;

            if (Mappings.TryGetValue(t, out factoryFunction))
            {
                return factoryFunction;
            }

            throw new ArgumentException("Type is unsupported in rocket queries. Sorry. Adding custom types is work in progress");
        }
    }
}