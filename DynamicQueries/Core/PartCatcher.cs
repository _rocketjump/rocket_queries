using System;

namespace DynamicQueries.Core
{
    public class PartCatcher : IPartCatcher
    {
        private const char lesserMarker = '<';
        private const char greaterMarker = '>';
        public Parts Catch(string queryPart)
        {
            string lesser = null;
            string greater = null;

            var array = queryPart.ToCharArray();

            for (var i = 0; i < array.Length; i++)
            {
                if (TryBuildPart(array, ref i, lesserMarker, greaterMarker, out string lpart))
                {
                    lesser = lpart;
                };

                if (TryBuildPart(array, ref i, greaterMarker, lesserMarker, out string gpart))
                {
                    greater = gpart;
                };

                // if (array[i] == lesserMarker && i != array.Length - 1)
                // {
                //     var start = ++i;
                //     while (i < array.Length && array[i] != greaterMarker)
                //     {
                //         i++;
                //     }
                //     lesser = new string(array, start, i - start);
                // }
            }

            return new Parts(greater, lesser);
        }

        private bool TryBuildPart(char[] array, ref int i, char thisMarker, char otherMarker, out string part)
        {
            part = null;
            if (i < array.Length - 1 && array[i] == thisMarker )
            {
                var start = ++i;
                while (i < array.Length && array[i] != otherMarker)
                {
                    i++;
                }
                part = new string(array, start, i - start);
                --i;
                return true;
            }
            return false;
        }
    }
}