namespace DynamicQueries.Core
{
    public class Parts
    {
        public Parts(string greater, string lesser)
        {
            Greater = greater;
            Lesser = lesser;
        }

        public string Greater { get; private set; }
        public string Lesser { get; private set; }
    }
}