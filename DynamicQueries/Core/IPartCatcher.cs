namespace DynamicQueries.Core
{
    public interface IPartCatcher
    {
        Parts Catch(string queryPart);
    }
}