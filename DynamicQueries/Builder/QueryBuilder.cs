using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DynamicQueries.QueryParts;

namespace DynamicQueries.Builder
{
    public class QueryBuilder<TModel>
    {
        private ParameterExpression parameter;
        private Func<Expression> first;
        private List<Func<Expression, Expression>> chain = new List<Func<Expression, Expression>>();

        private QueryBuilder(IQueryPart e, bool negation)
        {
            parameter = Expression.Parameter(typeof(TModel));
            SetFirst(e, negation);
        }

        private QueryBuilder()
        {
            parameter = Expression.Parameter(typeof(TModel));
        }

        public static QueryBuilder<TModel> Empty()
        {
            return new QueryBuilder<TModel>();
        }

        public static QueryBuilder<TModel> StartWithNot(IQueryPart e)
        {
            if (e == null)
                throw new ArgumentException($"Value cannot be null {nameof(e)}");

            return new QueryBuilder<TModel>(e, true);
        }

        public static QueryBuilder<TModel> StartWith(IQueryPart e)
        {
            if (e == null)
                throw new ArgumentException($"Value cannot be null {nameof(e)}");

            return new QueryBuilder<TModel>(e, false);
        }

        public QueryBuilder<TModel> Start(IQueryPart e, bool negation)
        {
            if (e == null)
                throw new ArgumentException($"Value cannot be null {nameof(e)}");

            SetFirst(e, negation);
            return this;
        }

        public QueryBuilder<TModel> And(IQueryPart e)
        {
            ThrowIfFirstNull(e);

            chain.Add((previous) =>
            {
                var member = Expression.PropertyOrField(parameter, e.PropertyName());
                return Expression.AndAlso(previous, e.GetPredicate<TModel>(member));
            });
            return this;
        }

        public QueryBuilder<TModel> Or(IQueryPart e)
        {
            ThrowIfFirstNull(e);

            chain.Add((previous) =>
                {
                    var member = Expression.PropertyOrField(parameter, e.PropertyName());
                    return Expression.OrElse(previous, e.GetPredicate<TModel>(member));
                });
            return this;
        }

        public QueryBuilder<TModel> AndNot(IQueryPart e)
        {
            ThrowIfFirstNull(e);

            chain.Add((previous) =>
            {
                var member = Expression.PropertyOrField(parameter, e.PropertyName());
                var not = Expression.Not(e.GetPredicate<TModel>(member));
                return Expression.AndAlso(previous, not);
            });

            return this;
        }

        public Expression<Func<TModel, bool>> AndImDone()
        {
            if (first == null)
            {
                throw new NotSupportedException("Cannot build query without any parts");
            }

            var expression = first();

            foreach (var e in chain)
            {
                var next = e(expression);
                expression = next;
            }

            return Expression.Lambda<Func<TModel, bool>>(expression, parameter);
        }

        private void SetFirst(IQueryPart e, bool negation)
        {
            if (e == null)
                throw new ArgumentException($"Value cannot be null {nameof(e)}");

            first = () =>
            {
                var member = Expression.PropertyOrField(parameter, e.PropertyName());
                if (negation == false)
                {

                    return e.GetPredicate<TModel>(member);
                }
                else
                {
                    return Expression.Not(e.GetPredicate<TModel>(member));
                }
            };
        }

        private void ThrowIfFirstNull(IQueryPart e)
        {
            if (first == null)
            {
                throw new NotSupportedException($"Trying to add {e.PropertyName()}. Please call Start methods before venturing forth");
            }

            if (e == null)
            {
                throw new ArgumentException($"Value cannot be null {nameof(e)}");
            }
        }
    }
}