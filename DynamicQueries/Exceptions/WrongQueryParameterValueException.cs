using System;

namespace DynamicQueries.Exceptions
{
    public class WrongQueryParameterValueException : Exception
    {
        public WrongQueryParameterValueException(string queryValue, Type queryPartType)
         : base($"Wrong query parameter value {queryValue} for {queryPartType}")
        {
        }
    }
}