using System;
using System.Linq.Expressions;

namespace DynamicQueries.QueryParts
{
    public interface IQueryPart
    {
         Expression<Func<TModel, bool>> GetLambda<TModel>();
         Expression GetPredicate<TModel>(MemberExpression property);

         string PropertyName();
    }
}