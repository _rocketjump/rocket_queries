using System;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace DynamicQueries.QueryParts
{
    public class IntegerQueryPart : RangeQueryPart
    {
        private static Regex greaterRegex = new Regex(@"(?<=\>)-?\d*");
        private static Regex lesserRegex = new Regex(@"(?<=\<)-?\d*");
        private static Regex integerRegex => new Regex(@"-?\d*");

        protected override Match Greater(string part)
        {
            var match = greaterRegex.Match(part);
            return new Match(match.Success, match.Value);
        }
        protected override Match Lesser(string part)
        {
            var match = lesserRegex.Match(part);
            return new Match(match.Success, match.Value);
        }
        protected override Match Equal(string part)
        {
            var match = integerRegex.Match(part);
            return new Match(match.Success, match.Value);
        }

        public IntegerQueryPart(string propertyName, string queryString) : base(propertyName, queryString)
        {
        }
    }
}