using System;
using System.Linq;
using System.Linq.Expressions;

namespace DynamicQueries.QueryParts
{
    public class EnumQueryPart : QueryPart
    {
        public EnumQueryPart(string propertyName, string queryString) : base(propertyName, queryString)
        {
        }

        public override bool TryGetPredicate<TModel>(string queryString, out Func<MemberExpression, Expression> predicate)
        {
            predicate = (property) =>
            {
                var enumValues = queryString.Split(',');
                var enumArray = enumValues.Select(x => ParseMe(x, property)).ToArray();
                var sadfa = Expression.NewArrayInit(property.Type, enumValues.Select(x => Expression.Constant(ParseMe(x, property))));


                //public static bool Contains<TSource>(this IEnumerable<TSource> source, TSource value);
                // var methodInfo = typeof(System.Linq.Enumerable).GetMethod("Contains", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                return Expression.Call(typeof(System.Linq.Enumerable), "Contains", new Type[] { property.Type }, sadfa, property);
            };

            return true;
        }
    }
}