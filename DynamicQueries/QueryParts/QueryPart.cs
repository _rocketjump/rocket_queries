using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;

namespace DynamicQueries.QueryParts
{
    public abstract class QueryPart : IQueryPart
    {
        protected string _queryString;

        protected string _propertyName;

        protected string[] _nested;

        public QueryPart(string propertyName, string queryString)
        {
            _queryString = queryString;

            var nestedChain = propertyName.Split('.');
            if (nestedChain.Length > 1)
            {
                _propertyName = nestedChain[0];
                _nested = new string[nestedChain.Length - 1];
                Array.Copy(nestedChain, 1, _nested, 0, _nested.Length);
            }
            else
            {

                _propertyName = propertyName;
            }
        }

        public Expression GetPredicate<TModel>(MemberExpression property)
        {
            ///TODO make this exceptionally nice
            if (string.IsNullOrEmpty(_propertyName) || _queryString == null)
            {
                throw new ArgumentException("Value cannot be null");
            }

            var nullChecks = new List<Expression>();

            if (Nullable.GetUnderlyingType(property.Type) != null || property.Type.IsValueType == false)
            {
                nullChecks.Add(Expression.NotEqual(property, Expression.Constant(null)));
            }

            if (_nested != null)
            {
                foreach (var nestedName in _nested)
                {
                    var nestedProperty = Expression.PropertyOrField(property, nestedName);
                    property = nestedProperty;
                    if (Nullable.GetUnderlyingType(nestedProperty.Type) != null || nestedProperty.Type.IsValueType == false)
                    {
                        nullChecks.Add(Expression.NotEqual(property, Expression.Constant(null)));
                    }
                }
            }

            Func<MemberExpression, Expression> getPredicate = null;

            if (!TryGetPredicate<TModel>(_queryString, out getPredicate))
            {
                throw new Exceptions.WrongQueryParameterValueException(_queryString, typeof(DecimalQueryPart));
            }

            Expression nullPredicate = null;
            foreach (var nullCheck in nullChecks)
            {
                if (nullPredicate == null)
                {
                    nullPredicate = nullCheck;
                }
                else
                {
                    nullPredicate = Expression.AndAlso(nullPredicate, nullCheck);
                }
            }
            if (_nested != null && nullPredicate != null)
            {
                return Expression.AndAlso(nullPredicate, getPredicate(property));
            }
            else
            {
                return getPredicate(property);
            }
        }

        public Expression<Func<TModel, bool>> GetLambda<TModel>()
        {
            ///TODO make this exceptionally nice
            if (string.IsNullOrEmpty(_propertyName) || _queryString == null)
            {
                throw new ArgumentException("Value cannot be null");
            }
            var parameter = Expression.Parameter(typeof(TModel));

            var property = Expression.PropertyOrField(parameter, _propertyName);

            var nullChecks = new List<Expression>();

            if (Nullable.GetUnderlyingType(property.Type) != null || property.Type.IsValueType == false)
            {
                nullChecks.Add(Expression.NotEqual(property, Expression.Constant(null)));
            }

            if (_nested != null)
            {
                foreach (var nestedName in _nested)
                {
                    var nestedProperty = Expression.PropertyOrField(property, nestedName);
                    property = nestedProperty;
                    if (Nullable.GetUnderlyingType(nestedProperty.Type) != null || nestedProperty.Type.IsValueType == false)
                    {
                        nullChecks.Add(Expression.NotEqual(property, Expression.Constant(null)));
                    }
                }
            }

            Func<MemberExpression, Expression> getPredicate = null;

            if (!TryGetPredicate<TModel>(_queryString, out getPredicate))
            {
                throw new Exceptions.WrongQueryParameterValueException(_queryString, typeof(DecimalQueryPart));
            }

            Expression nullPredicate = null;
            foreach (var nullCheck in nullChecks)
            {
                if (nullPredicate == null)
                {
                    nullPredicate = nullCheck;
                }
                else
                {
                    nullPredicate = Expression.AndAlso(nullPredicate, nullCheck);
                }
            }
            if (_nested != null && nullPredicate != null)
            {
                return Expression.Lambda<Func<TModel, bool>>(Expression.AndAlso(nullPredicate, getPredicate(property)), new ParameterExpression[] { parameter });
            }
            else
            {
                return Expression.Lambda<Func<TModel, bool>>(getPredicate(property), new ParameterExpression[] { parameter });
            }
        }

        public abstract bool TryGetPredicate<TModel>(string queryString, out Func<MemberExpression, Expression> predicate);
        protected object ParseMe(string value, MemberExpression type)
        {
            TypeConverter conv = TypeDescriptor.GetConverter(type.Type);
            var converted = conv.ConvertFrom(value);
            var tt = converted.GetType();
            return converted;
        }

        public string PropertyName()=> _propertyName;
    }
}