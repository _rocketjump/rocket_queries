using System;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace DynamicQueries.QueryParts
{
    public abstract class RangeQueryPart : QueryPart
    {
        protected RangeQueryPart(string propertyName, string queryString) : base(propertyName, queryString)
        {
        }

        protected abstract Match Greater(string part);
        protected abstract Match Lesser(string part);
        protected abstract Match Equal(string part);

        public override bool TryGetPredicate<TModel>(string queryString, out Func<MemberExpression, Expression> predicate)
        {
            predicate = null;
            var equalMatch = Equal(queryString);
            if (equalMatch.Success && equalMatch.Value.Equals(queryString))
            {
                predicate = (property) =>
                {
                    //catch this parse and property type mismatch
                    return Expression.Call(property, "Equals", null, new[] { Expression.Constant(ParseMe(queryString, property), property.Type) });
                };
                return true;
            }

            var greaterMatch = Greater(queryString);
            var lesserMatch = Lesser(queryString);

            if (!greaterMatch.Success && !lesserMatch.Success)
            {
                return false;
            }

            if (greaterMatch.Success && lesserMatch.Success)
            {
                predicate = (property) =>
                {

                    var left = Expression.GreaterThanOrEqual(property, Expression.Constant(ParseMe(greaterMatch.Value, property), property.Type));
                    var right = Expression.LessThanOrEqual(property, Expression.Constant(ParseMe(lesserMatch.Value, property), property.Type));

                    var expression = Expression.AndAlso(left, right);
                    DecorateIfNullable(property, ref expression);
                    return expression;
                };
                return true;
            }

            if (greaterMatch.Success)
            {
                predicate = (property) =>
                {
                    var expression = Expression.GreaterThanOrEqual(property, Expression.Constant(ParseMe(greaterMatch.Value, property), property.Type));
                    DecorateIfNullable(property, ref expression);
                    return expression;
                };
                return true;
            }

            if (lesserMatch.Success)
            {
                predicate = (property) =>
                {
                    var expression = Expression.LessThanOrEqual(property, Expression.Constant(ParseMe(lesserMatch.Value, property), property.Type));
                    DecorateIfNullable(property, ref expression);
                    return expression;
                };

                return true;
            }
            return false;
        }

        private void DecorateIfNullable(MemberExpression property, ref BinaryExpression predicate)
        {
            if (Nullable.GetUnderlyingType(property.Type) != null)
            {
                var nullPredicate = Expression.NotEqual(property, Expression.Constant(null));
                predicate = Expression.AndAlso(nullPredicate, predicate);
            }
        }
    }
}