using System;
using DynamicQueries.Core;

namespace DynamicQueries.QueryParts
{
    public class DateTimeQueryPart : RangeQueryPart
    {
        private readonly IPartCatcher _catcher;
        public DateTimeQueryPart(string propertyName, string queryString) : base(propertyName, queryString)
        {
            _catcher = new PartCatcher();
        }

        protected override Match Equal(string part)
        {
            if (DateTime.TryParse(part, out var date))
            {
                return new Match(true, part);
            }
            return new Match(false, null);
        }

        protected override Match Greater(string part)
        {
            var match = _catcher.Catch(part);
            if (string.IsNullOrEmpty(match.Greater))
            {
                return new Match(false, null);
            }

            return new Match(true, match.Greater);
        }

        protected override Match Lesser(string part)
        {
            var match = _catcher.Catch(part);
            if (string.IsNullOrEmpty(match.Lesser))
            {
                return new Match(false, null);
            }

            return new Match(true, match.Lesser);
        }
    }
}