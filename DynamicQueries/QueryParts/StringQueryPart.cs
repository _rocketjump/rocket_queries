using System;
using System.Linq.Expressions;

namespace DynamicQueries.QueryParts
{
    //?propertyName=SomeValue
    public class StringQueryPart : QueryPart
    {
        public StringQueryPart(string propertyName, string queryString) : base(propertyName, queryString)
        {
        }
        public override bool TryGetPredicate<TModel>(string queryString, out Func<MemberExpression, Expression> predicate)
        {
            predicate = null;

            if (string.IsNullOrEmpty(queryString))
            {
                return false;
            }

            predicate = (property) =>
            {
                return Expression.Call(
                   property,
                   "Contains",
                   null,
                   new[] { Expression.Constant(_queryString), Expression.Constant(StringComparison.InvariantCulture) }
                   );
            };
            return true;
        }
    }
}