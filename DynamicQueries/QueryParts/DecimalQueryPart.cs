using System;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace DynamicQueries.QueryParts
{
    //?propertyName=>SomeValue<OtherValue
    //What about greater or equals?
    public class DecimalQueryPart : RangeQueryPart
    {
        protected static Regex greaterRegex = new Regex(@"(?<=\>)-?\d*\.?\d*");
        private static Regex lesserRegex = new Regex(@"(?<=\<)-?\d*\.?\d*");
        protected static Regex decimalRegex => new Regex(@"-?\d*\.?\d*");
        
        protected override Match Greater(string part)
        {
            var match = greaterRegex.Match(part);
            return new Match(match.Success, match.Value);
        }
        protected override Match Lesser(string part)
        {
            var match = lesserRegex.Match(part);
            return new Match(match.Success, match.Value);
        }
        protected override Match Equal(string part)
        {
            var match = decimalRegex.Match(part);
            return new Match(match.Success, match.Value);
        }

        public DecimalQueryPart(string propertyName, string queryString) : base(propertyName, queryString)
        {
            //what if we get null values?
        }
    }
}