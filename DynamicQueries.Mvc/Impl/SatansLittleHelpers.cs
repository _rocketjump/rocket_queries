using System;

namespace DynamicQueries.Mvc
{
    public static class SantasLittleHelpers
    {
        public static string CapitalizeFirstLetter(string lowerCase)
        {
            var UpperCase = new char[lowerCase.Length];

            for (var i = 0; i < lowerCase.Length; i++)
            {
                if (i == 0)
                {
                    UpperCase[i] = Char.ToUpper(lowerCase[0]);
                }
                else
                {
                    UpperCase[i] = lowerCase[i];
                }
            }
            return new string(UpperCase);
        }
    }
}