using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using DynamicQueries.Builder;
using DynamicQueries.QueryParts;
using Microsoft.AspNetCore.Http;

namespace DynamicQueries.Mvc
{
    public class DefaultQuerySource : IQuerySource
    {
        private readonly QueryOperatorPart[] _operators;
        public DefaultQuerySource()
        {
            _operators = new QueryOperatorPart[]{
                new NotQueryOperatorPart(),
                new OrQueryOperatorPart(),
                new AndQueryOperatorPart(),
                new StartQueryOperatorPart(),
                new NegationStartQueryOperatorPart()
            };
        }

        public bool TryGet<TModel>(string queryString, Dictionary<string, Func<string, string, IQueryPart>> definitions, out Expression<Func<TModel, bool>> query)
        {
            query = null;

            if (string.IsNullOrEmpty(queryString))
            {
                query = Expression.Lambda<Func<TModel, bool>>(Expression.Constant(true), Expression.Parameter(typeof(TModel)));
                return true;
            }

            StringBuilder builder = new StringBuilder();

            var queryArray = queryString.ToArray();

            var firstChar = queryArray[0];

            QueryBuilder<TModel> queryBuilder = QueryBuilder<TModel>.Empty();

            var currentOperator = _operators.First(x => x.IsEnabled(firstChar, true));
            IQueryPart currentQuerrry = null;

            for (var i = 0; i < queryArray.Length; i++)
            {
                var newOperator = _operators.FirstOrDefault(x => x.IsEnabled(queryArray[i], i == 0));

                if (newOperator != null)
                {
                    if (i != 0)
                    {
                        var p = builder.ToString().Split('=');
                        currentQuerrry = definitions[p[0]](SantasLittleHelpers.CapitalizeFirstLetter(p[0].Replace("_", ".")), p[1]);
                        currentOperator.Join(queryBuilder, currentQuerrry);
                        builder.Clear();
                        currentOperator = newOperator;
                    }

                    if (currentOperator.Skip())
                    {
                        i++;
                    }
                }

                builder.Append(queryArray[i]);
            }
            var partInArray = builder.ToString().Split('=');
            currentQuerrry = definitions[partInArray[0]](SantasLittleHelpers.CapitalizeFirstLetter(partInArray[0].Replace("_", ".")), partInArray[1]);
            currentOperator.Join(queryBuilder, currentQuerrry);
            query = queryBuilder.AndImDone();
            return true;
        }
    }
}