using System;
using System.Text;
using DynamicQueries.Builder;
using DynamicQueries.QueryParts;

namespace DynamicQueries.Mvc
{
    public abstract class QueryOperatorPart
    {
        public QueryOperatorPart()
        {
        }

        public abstract bool IsEnabled(char s, bool first);
        public abstract void Join<TModel>(QueryBuilder<TModel> builder, IQueryPart part);
        public abstract bool Skip();
    }

    public class AndQueryOperatorPart : QueryOperatorPart
    {
        public override void Join<TModel>(QueryBuilder<TModel> builder, IQueryPart part)
        {
            builder.And(part);
        }

        public override bool IsEnabled(char s, bool first)
        {
            return first == false && '&'.Equals(s);
        }

        public override bool Skip() => true;
    }

    public class OrQueryOperatorPart : QueryOperatorPart
    {
        public override void Join<TModel>(QueryBuilder<TModel> builder, IQueryPart part)
        {
            builder.Or(part);
        }

        public override bool IsEnabled(char s, bool first)
        {
            return first == false && '|'.Equals(s);
        }
        public override bool Skip() => true;
    }

    public class NotQueryOperatorPart : QueryOperatorPart
    {
        public override void Join<TModel>(QueryBuilder<TModel> builder, IQueryPart part)
        {
            builder.AndNot(part);
        }

        public override bool IsEnabled(char s, bool first)
        {
            return first == false && '!'.Equals(s);
        }
        public override bool Skip() => true;
    }

    public class StartQueryOperatorPart : QueryOperatorPart
    {
        public override void Join<TModel>(QueryBuilder<TModel> builder, IQueryPart part)
        {
            builder.Start(part, false);
        }

        public override bool IsEnabled(char s, bool first)
        {
            return first == true && '!'.Equals(s) == false;
        }
        public override bool Skip() => false;
    }

    public class NegationStartQueryOperatorPart : QueryOperatorPart
    {
        public override void Join<TModel>(QueryBuilder<TModel> builder, IQueryPart part)
        {
            builder.Start(part, true);
        }

        public override bool IsEnabled(char s, bool first)
        {
            return first == true && '!'.Equals(s);
        }

        public override bool Skip() => true;
    }
}