using System;
using Microsoft.AspNetCore.Mvc.Filters;

namespace DynamicQueries.Mvc
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class QueryAttribute : Attribute, IFilterMetadata
    {
        public string Name { get; private set; }
        public Type Type { get; private set; }

        public QueryAttribute(string name, Type type)
        {
            Name = name;
            Type = type;
        }
    }
}