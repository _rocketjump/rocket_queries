using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DynamicQueries.QueryParts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace DynamicQueries.Mvc
{
    public class QueryController : Controller
    {
        private readonly DynamicQueries.IFactory _factory;
        private Dictionary<string, Func<string, string, IQueryPart>> Definitions;
        private readonly IQuerySource _querySource;

        public QueryController(IQuerySource querySource, DynamicQueries.IFactory factory)
        {
            _factory = factory;
            _querySource = querySource;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!TryGetParameterDefinitions(context, out Definitions))
            {
                return;
            }
        }

        protected bool TryBuildQueryFromQueryString<TModel>(out Expression<Func<TModel, bool>> query)
        {
            query = null;

            var queryString = Request.QueryString;

            if (!queryString.HasValue)
            {
                return false;
            }

            var queryParameter =  HttpUtility.HtmlDecode(Request.Query["query"]);

            if (_querySource.TryGet<TModel>(queryParameter, Definitions, out query))
            {
                return true;
            }
            return false;
        }

        private bool TryGetParameterDefinitions(ActionExecutingContext context, out Dictionary<string, Func<string, string, IQueryPart>> definitions)
        {
            definitions = new Dictionary<string, Func<string, string, IQueryPart>>();

            foreach (var filter in context.Filters)
            {
                if (filter is QueryAttribute)
                {
                    var queryAttributeDefinition = filter as QueryAttribute;
                    definitions.Add(queryAttributeDefinition.Name, _factory.Get(queryAttributeDefinition.Type));
                }
            }
            return definitions.Any();
        }
    }
}
