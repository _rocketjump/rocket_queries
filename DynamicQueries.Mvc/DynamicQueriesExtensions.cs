using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace DynamicQueries.Mvc
{
    public static class DynamicQueriesExtensions
    {
        public static void AddDynamicQueries(this IServiceCollection services)
        {
            services.AddScoped<IFactory, Factory>();
            services.AddScoped<IQuerySource, DefaultQuerySource>();
        }
    }
}