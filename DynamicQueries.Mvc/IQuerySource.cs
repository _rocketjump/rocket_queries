using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DynamicQueries.QueryParts;
using Microsoft.AspNetCore.Http;

namespace DynamicQueries.Mvc
{
    public interface IQuerySource
    {
        bool TryGet<TModel>(string queryString, Dictionary<string, Func<string, string, IQueryPart>> definitions, out Expression<Func<TModel, bool>> query);
    }
}