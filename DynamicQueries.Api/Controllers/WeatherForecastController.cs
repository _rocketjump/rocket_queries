﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DynamicQueries.Mvc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DynamicQueries.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : QueryController
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IQuerySource querySource, DynamicQueries.IFactory factory) : base(querySource, factory)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpGet]
        [Route("parties")]
        [QueryAttribute("owner_Name", typeof(string))]
        [QueryAttribute("cost", typeof(decimal))]
        public IEnumerable<Party> MyFirstQuery()
        {

            var parties = new List<Party>(){
                new Party(){
                    Id = 1,
                    Name = "Williams birthday",
                    Cost = 2501
                },
                new Party(){
                    Id = 2,
                    Name ="Double boom",
                    Cost = 1000
                },
                    new Party(){
                    Id = 3,
                    Name ="Boom boom boom boom",
                    Cost = 999
                },
                  new Party(){
                    Id = 4,
                    Name ="New years eve",
                    Cost = 12344
                },
                   new Party(){
                    Id = 6,
                    Name ="Double boom boom",
                    Tickets = 101,
                    Owner = new Person(){
                        Name = "Jill"
                    }
                },
            };

            Expression<Func<Party, bool>> func = null;
            TryBuildQueryFromQueryString<Party>(out func);
            return parties.Where(func.Compile()).ToList();
        }

        public class Party
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int Tickets { get; set; }
            public int TicketsSold { get; set; }
            public DateTime PartyDate { get; set; }
            public decimal Cost { get; set; }
            public Nullable<decimal> Refund { get; set; }
            public PartyStyle Theme { get; set; }

            public Person Owner { get; set; }
        }

        public enum PartyStyle
        {
            Whatever = 0,
            CrazyJunkHEad = 1,
            Byobbbc = 2,
        }

        public class Person
        {
            public string Name { get; set; }
            public DateTime? BirthDate { get; set; }
        }
    }
}
